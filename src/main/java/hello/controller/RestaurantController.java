package hello.controller;


import hello.dao.RestaurantDao;
import hello.model.Restaurant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class RestaurantController {

    @Autowired
    private RestaurantDao restaurantRepository;

    @GetMapping(path = "/add")
    public @ResponseBody String addNewRestaurant (@RequestParam String name, String address){

        Restaurant n = new Restaurant();
        n.setName(name);
        n.setAddress(address);
        restaurantRepository.save(n);
        return "Saved";
    }

    @GetMapping(path = "/all")
    public @ResponseBody List<Restaurant> getAllRestaurants(){
        return restaurantRepository.findAll();
    }

}
