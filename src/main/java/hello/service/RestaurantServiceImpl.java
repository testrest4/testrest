package hello.service;

import hello.dao.RestaurantDao;
import hello.model.Restaurant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RestaurantServiceImpl implements RestaurantService {

    @Autowired
    private RestaurantDao restaurantDao;

    @Override
    public void save(Restaurant restaurant) {
        restaurantDao.save(restaurant);
    }

    @Override
    public List<Restaurant> findAll() {
        return restaurantDao.findAll();
    }
}
