package hello.service;

import hello.model.Restaurant;

import java.util.List;

public interface RestaurantService {

    void save(Restaurant restaurant);
    List<Restaurant> findAll();
}
