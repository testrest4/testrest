package hello.dao;


import hello.model.Restaurant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public class RestaurantDaoImpl implements RestaurantDao {


    @Autowired
    private EntityManager entityManager;

    @Override
    public void save(Restaurant restaurant) {
        entityManager.persist(restaurant);
    }

    @Override
    public List<Restaurant> findAll() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Restaurant> cq = cb.createQuery(Restaurant.class);
        Root<Restaurant> root = cq.from(Restaurant.class);
        cq.select(root);
        TypedQuery<Restaurant> query = entityManager.createQuery(cq);
        return query.getResultList();
    }
}
