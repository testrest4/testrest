package hello.dao;

import hello.model.Restaurant;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface RestaurantDao {
    void save(Restaurant restaurant);
    List<Restaurant> findAll();
}
