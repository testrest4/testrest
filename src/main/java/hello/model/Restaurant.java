package hello.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "restaurants")
public class Restaurant implements Serializable {

    public Integer getId_Restaurant() {
        return Id_Restaurant;
    }

    public void setId_Restaurant(Integer id_Restaurant) {
        Id_Restaurant = id_Restaurant;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    private Integer Id_Restaurant;
    private String Name;
    private String Address;



}
