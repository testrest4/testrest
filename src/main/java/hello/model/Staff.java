package hello.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "staff")
public class Staff implements Serializable {

    @Id
    private Integer Id_Restaurant;
    private String Name;

    public Integer getId_Restaurant() {
        return Id_Restaurant;
    }

    public void setId_Restaurant(Integer id_Restaurant) {
        Id_Restaurant = id_Restaurant;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSurname() {
        return Surname;
    }

    public void setSurname(String surname) {
        Surname = surname;
    }

    private String Surname;




}
